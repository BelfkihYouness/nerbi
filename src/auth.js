import Axios from "axios";

const API = Axios.create({
    baseURL: '/api',
    timeout: 3000,
});

let token = '';

setToken(localStorage.getItem('token') || '');

export function getToken() {
    return token;
}

export function setToken( value ) {
    if ( value === '' ) return;
    token = value;
    localStorage.setItem('token', value);
}

export function isSignedIn() {
    return token !== '';
}

export function signOut() {
    localStorage.removeItem('token');
    token = '';
}
export async function signIn(email, password) {
    return API.post('/token', { email, password });
}

export function signUp(email, password ) {
    return API.post('/register', { email, password });
}