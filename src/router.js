import Vue from 'vue';
import Router from 'vue-router';

import Home from './views/Home';
import SignIn from './views/SignIn';
import SignUp from './views/SignUp';
import Favorites from './views/Favorites';
import Shops from './views/Shops';

import * as auth from "./auth";

Vue.use(Router);

const beforeUserEnter = (to, from, next) => {
    if ( auth.getToken() === '' ) {
        next(false);
        router.push('/');
    } else {
        next();
    }
};

const beforeHomeEnter = (to, from, next) => {
    if ( auth.getToken() !== '' ) {
        next(false);
        router.push('/near');
    } else {
        next();
    }
};

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: { title: 'NERBI' },
            beforeEnter: beforeHomeEnter
        },
        {
            path: '/signin',
            name: 'SignIn',
            component: SignIn,
            meta: { title: 'NERBI - Sign in' },
            beforeEnter: beforeHomeEnter
        },
        {
            path: '/signup',
            name: 'SignUp',
            component: SignUp,
            meta: { title: 'NERBI - Sign up' },
            beforeEnter: beforeHomeEnter
        },
        {
            path: '/near',
            name: 'Shops',
            component: Shops,
            meta: { title: 'NERBI - Nearby Shops' },
            beforeEnter: beforeUserEnter
        },
        {
            path: '/preferred',
            name: 'Favorites',
            component: Favorites,
            meta: { title: 'NERBI - Preferred Shops' },
            beforeEnter: beforeUserEnter
        },
        {
            path: '/signout',
            meta: { title: 'NERBI - Logging out ...' },
            beforeEnter(_, __, next) {
                auth.signOut();
                router.push('/');
                next(false);
            }
        },
        {
            path: '/*',
            redirect: '/'
        }
    ],
    mode: "history"
});

export default router;