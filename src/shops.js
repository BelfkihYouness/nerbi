import Axios from "axios";

import * as auth from './auth';

const API = Axios.create({
    baseURL: '/api/shops',
    timeout: 3000,
});

// NOTE: Let's say longitude and latitude are tied to the device's location.

export function getNearbyShops() {
    return API.post(`/`, {
        token: auth.getToken(),
        longitude: 0,
        latitude: 0
    });
}

export function hideShop( id ) {
    return API.post('/hide', { token: auth.getToken(), id });
}

export function getFavoriteShops() {
    return API.post('/saved', {
        token: auth.getToken(),
        longitude: 0,
        latitude: 0
    });
}

export function addFavoriteShop( id ) {
    return API.post('/save', { token: auth.getToken(), id });
}

export function removeFavoriteShop( id ) {
    return API.post('/unsave', { token: auth.getToken(), id });
}