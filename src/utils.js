export function humanizeDistance( distance ) {
    if ( distance < 100 )
        return 'Few meters';
    if ( distance > 1000 )
        return `${ ((distance / 1000) * 10 | 0) / 10 }Km`;
    if ( distance <= 900 )
        return 'Nearly a Kilometer';
    if ( distance >= 300 )
        return 'Few hundred meters';
    return `Nearly ${Math.floor(distance / 100)}m`;
}